import json
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UserListTestCase(APITestCase):
    base_url = reverse('users-list')

    def test_users_list(self):
        User.objects.create_user(username='Paul', email="paul@gmail.com", password="password123")
        User.objects.create_user(username='Nolan', email="nolan@gmail.com", password="password123")
        response = self.client.get(self.base_url, format='json')

        data = json.loads(json.dumps(response.data))
        # Check if the users are present in the response data

        self.assertIn('Paul', data['results'][0]['username'])
        self.assertIn('Nolan', data['results'][1]['username'])
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserCreateTestCase(APITestCase):
    base_url = reverse('users-list')

    def test_create_user(self):
        data = {
            "username": "Kyle",
            "email": "kyle@gmail.com",
            "password": "admin12345",
            "first_name": "Kyle",
            "last_name": "Walker"
        }

        response = self.client.post(self.base_url, data, format='json')
        user = User.objects.filter(username='Kyle').exists()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertTrue(user)


class UserUpdateTestCase(APITestCase):
    def test_update_user(self):
        # Create a user for testing
        user = User.objects.create_user(username='Nolan', email="nolan@gmail.com", password="password123",
                                        first_name="Nolan", last_name="Nix")
        base_url = reverse('users-detail',
                           args=[user.id])

        data = {
            "first_name": "Michael",
            "last_name": "Jordan",
            "username": "Nolan",
            "password": "password123"
        }

        response = self.client.put(base_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Fetch the updated user from the database
        updated_user = User.objects.get(id=user.id)

        # Verify that the user's first_name and last_name have been updated
        self.assertEqual(updated_user.first_name, "Michael")
        self.assertEqual(updated_user.last_name, "Jordan")


class DeleteUserTestCase(APITestCase):
    def test_delete_user(self):
        user = User.objects.create_user(username='brandon', email="brandon@gmail.com", password="password123",
                                        first_name="Brandon", last_name="Lux")
        base_url = reverse('users-detail',
                           args=[user.id])

        response = self.client.delete(base_url, format='json')
        user = User.objects.filter(id=user.id).exists()
        self.assertFalse(user)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
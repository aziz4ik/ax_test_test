from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UserRegistrationTest(APITestCase):
    base_url = reverse('user_registration')

    def test_user_registration(self):
        data = {
            "username": "felix",
            "password": "password123",
            "first_name": "Felix",
            "last_name": "Jonson",
            "email": "felix@gmail.com"
        }

        response = self.client.post(self.base_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertIn('access_token', response.data)
        self.assertIn('refresh_token', response.data)
        self.assertEqual(response.data['email'], data['email'])
        self.assertEqual(response.data['first_name'], data['first_name'])
        self.assertEqual(response.data['last_name'], data['last_name'])

        # Optional: Check if the user is created in the database
        user_exists = User.objects.filter(username=data['username']).exists()
        self.assertTrue(user_exists)

    def test_user_registration_incorrect_credentials(self):
        data = {
            # Assume email is entered incorrectly
            "email": "fenix3",
            "password": "password123",
            "first_name": "Wen",
            "last_name": "Baz",
        }

        response = self.client.post(self.base_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UserLoginTestCase(APITestCase):
    base_url = reverse('user_login')

    def test_user_login(self):
        user = User.objects.create_user(username='Paul', email="paul@gmail.com", password="password123")

        data = {
            "username": "Paul",
            "password": "password123"
        }

        response = self.client.post(self.base_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('access_token', response.data)
        self.assertIn('refresh_token', response.data)

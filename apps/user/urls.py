from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.user.views import UserViewSet, UserCreateView, UserLoginView

router = DefaultRouter()
router.register('users', UserViewSet, 'users')

urlpatterns = [
    path('', include(router.urls)),
    path('user_auth_registration/', UserCreateView.as_view(), name='user_registration'),
    path('user_auth_login/', UserLoginView.as_view(), name='user_login')
]

# app_name = 'user'

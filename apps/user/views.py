from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework import viewsets, generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken

from apps.user.serializers.user import UserSerializer, UserCreateSerializer

from django_filters.rest_framework import DjangoFilterBackend


class UserViewSet(viewsets.ModelViewSet):
    """
    *GET* - returns list of users
    *GET {id}* - returns one user with id specified in the request
    *POST* - creates a new user
    *PUT {id}* - updates employee when id of user is sent
    *DELETE {id}* - deletes user when id is sent
    """

    model = User
    queryset = User.objects.all()
    serializer_class = UserSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['first_name']
    ordering = ['last_name']


class UserCreateView(generics.CreateAPIView):
    """
    post:
    Creates user if all parameters are correct
    and returns access and refresh token
    """
    queryset = User.objects.all()
    serializer_class = UserCreateSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        # Get the access token and refresh token
        refresh = RefreshToken.for_user(serializer.instance)
        access_token = str(refresh.access_token)
        refresh_token = str(refresh)

        # Add the access token and refresh token to the response data
        response_data = serializer.data
        response_data['access_token'] = access_token
        response_data['refresh_token'] = refresh_token

        return Response(response_data, status=status.HTTP_201_CREATED, headers=headers)


class UserLoginView(APIView):
    """
    post:
    User login view, accepts two parameters
    *username*
    *password*
    If both username and password matches returns access and refresh token
    """
    def post(self, request, format=None):
        password = request.data.get('password')
        username = request.data.get('username')

        user = authenticate(username=username, password=password)

        if user:
            refresh = RefreshToken.for_user(user)
            access_token = str(refresh.access_token)
            refresh_token = str(refresh)

            response_data = {
                'access_token': access_token,
                'refresh_token': refresh_token,
                'user_first_name': user.first_name,
                'user_last_name': user.last_name,
                'user_id': user.id,
            }

            return Response(response_data, status=status.HTTP_200_OK)
        else:
            return Response({'error': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
